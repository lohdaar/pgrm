#include <unistd.h>

int	my_putstr (char *str) {
	int i = 0;
	while (str[i] != '\0') {
		my_putchar(str[i]);
		i = i + 1;
	}
	my_putchar('\n');
	return 0;
}
